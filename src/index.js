import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";

const pizzaData = [
  {
    name: "Focaccia",
    ingredients: "Bread with italian olive oil and rosemary",
    price: 6,
    photoName: "pizzas/focaccia.jpg",
    soldOut: false,
  },
  {
    name: "Pizza Margherita",
    ingredients: "Tomato and mozarella",
    price: 10,
    photoName: "pizzas/margherita.jpg",
    soldOut: false,
  },
  {
    name: "Pizza Spinaci",
    ingredients: "Mozarella, spinach, ham, and ricotta cheese",
    price: 12,
    photoName: "pizzas/spinaci.jpg",
    soldOut: false,
  },
  {
    name: "Prosciutto e Funghi",
    ingredients: "Tomato, mozarella, ham, mushrooms",
    price: 12,
    photoName: "pizzas/funghi.jpg",
    soldOut: false,
  },
  {
    name: "Salamino Piccante",
    ingredients: "Tomato, mozarella, basilico, and pepperoni",
    price: 15,
    photoName: "pizzas/salamino.jpg",
    soldOut: true,
  },
  {
    name: "Pizza Lasagna",
    ingredients: "Tomato, mozarella, ham, basilic, and pecorino cheese",
    price: 18,
    photoName: "pizzas/prosciutto.jpg",
    soldOut: false,
  },
];

function App() {
  return (
    <div className="container">
      <Header />
      <Menu />
      <Footer />
    </div>
  );
}

function Header() {
  const style = {};
  return (
    <header className="header">
      <h1 style={style}>Pizzeria La Bella Napoli</h1>
    </header>
  );
}

function Menu() {
  const pizzas = pizzaData;

  const numOfPizzas = pizzas.length;

  return (
    <main className="menu">
      <h2>Menu</h2>

      {numOfPizzas > 0 ? (
        <>
          <p>Eat like Italian, enjoy "la dolce vita"</p>
          <ul className="pizzas">
            {pizzas.map((pizza) => (
              <Pizza key={pizza.name} pizzaObj={pizza} />
            ))}
          </ul>
        </>
      ) : (
        <p> We are still working on our menu. Please come back later.</p>
      )}
    </main>
  );
}

function Pizza({ pizzaObj }) {
  //console.log(pizzaObj);

  return (
    <li className={`pizza ${pizzaObj.soldOut ? "sold-out" : ""}`}>
      <img src={pizzaObj.photoName} alt={pizzaObj.name} />
      <div>
        <h3>{pizzaObj.name}</h3>
        <p>{pizzaObj.ingredients}</p>
        <span className="sold-out">
          {pizzaObj.soldOut ? "SOLD OUT" : "Price: " + pizzaObj.price + " EUR"}
        </span>
      </div>
    </li>
  );
}

function Footer() {
  const hour = new Date().getHours();
  const openHour = 17;
  const closeHour = 24;
  const isOpen = hour >= openHour && hour < closeHour;

  return (
    <footer className="footer">
      {isOpen ? (
        <Order closeAt={closeHour} openAt={openHour} />
      ) : (
        <div className="order">
          <p>We are currently closed.</p>
          <p>
            You are welcome to visit us between {openHour}:00 - {closeHour}:00,
            or order a pizza online. We will fulfill your order during the open
            hours.
          </p>
        </div>
      )}
    </footer>
  );
}

function Order({ closeAt, openAt }) {
  return (
    <div className="order">
      <p>
        We are open {openAt}:00 - {closeAt}:00. Come visit us or order from web.
      </p>
      <button className="btn">Order a pizza</button>
    </div>
  );
}

//React v18 or later
const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
