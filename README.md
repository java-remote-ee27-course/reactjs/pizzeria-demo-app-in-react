# Pizzeria Demo App

My pizzeria demo app in React.

## Run project

Git clone the project.
In the project directory, you can run: `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

## Screenshots

![Pizzeria-demo-app](./public/assets/pizzeria1.png)
